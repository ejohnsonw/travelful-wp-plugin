<?php
/**
 * Travelful Flight Search and Booking Plugin .
 *
 *
 * @package Travelful Flight Search and Booking Plugin
 * @author Travelful
 * @license GPL-2.0+
 * @link http://travelful.co/tag/wordpress-beginner/
 * @copyright 2017 Travelful, LLC. All rights reserved.
 *
 *            @wordpress-plugin
 *            Plugin Name: Travelful Flight Search and Booking Plugin
 *            Plugin URI: http://travelful.co/tag/wordpress-beginner/
 *            Description: Travelful Flight Search and Booking Plugin.
 *            Version: 0.3
 *            Author: Travelful
 *            Author URI: http://travelful.co/
 *            Text Domain: travelful-plugin
 *            Contributors: Travelful
 *            License: GPL-2.0+
 *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

/**
 * Adding Submenu under Settings Tab
 *
 * @since 1.0
 */
 error_reporting(E_ERROR);
function travelful_add_menu() {
	add_submenu_page ( "options-general.php", "Travelful Plugin", "Travelful Plugin", "manage_options", "travelful-plugin", "travelful_options" );
}
add_action ( "admin_menu", "travelful_add_menu" );


add_action('init', 'register_script');
function register_script() {
    wp_register_script( 'airports2', plugins_url('/js/airports2.js', __FILE__) );
    wp_register_script( 'response', plugins_url('/js/response.js', __FILE__) );
    wp_deregister_script('jquery');
    wp_register_script( 'jquery', 'https://code.jquery.com/jquery-2.2.4.min.js' );
    wp_register_script( 'jqueryui', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('jquery') );
    wp_register_script( 'jqueryvalidate','https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js' , array('jquery') );
    wp_register_script( 'travelful', plugins_url('/js/travelful.js', __FILE__), array('jquery','airports2') );

    wp_register_style( 'bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
    wp_register_style( 'theme','https://code.jquery.com/ui/1.11.4/themes/ui-lightness/jquery-ui.css');
    wp_register_style( 'font-awesome', plugins_url('/css/font-awesome.css', __FILE__), false, '1.0.0', 'all');
    wp_register_style( 'tiptop', 'http://www.jqueryscript.net/css/jquerysctipttop.css');

    wp_register_style( 'style', plugins_url('/css/style.css', __FILE__), false, '1.0.0', 'all');
}

add_action('wp_enqueue_scripts', 'enqueue_style');
function enqueue_style(){
    wp_enqueue_style( 'bootstrap' );
    wp_enqueue_style( 'font-awesome' );
    wp_enqueue_style( 'tiptop' );
    wp_enqueue_style( 'theme' );
	wp_enqueue_style( 'style' );
    wp_enqueue_script('airports2');
    wp_enqueue_script('response');
    wp_enqueue_script("jquery");
    wp_enqueue_script('jqueryui');
    wp_enqueue_script('jqueryvalidate');
	wp_enqueue_script('travelful');

}

/**
 * Setting Page Options
 * - add setting page
 * - save setting page
 *
 * @since 1.0
 */
function travelful_options() {
	?>
    <div class="wrap">
        <h1>
            Travelful
            <a href="http://travelful.co/" target="_blank"></a>
        </h1>

        <form method="post" action="options.php">
            <?php
	settings_fields ( "travelful_plugin_config" );
	do_settings_sections ( "travelful-plugin" );
	submit_button ();
	?>
        </form>

    </div>

    <?php
}

/**
 * Init setting section, Init setting field and register settings page
 *
 * @since 1.0
 */
function travelful_plugin_settings() {
	add_settings_section ( "travelful_plugin_config", "", null, "travelful-plugin" );
	add_settings_field ( "travelful-event-uuid", "Event uuid", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-uuid','text'));
	add_settings_field ( "travelful-event-name", "Name", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-name','text'));
	add_settings_field ( "travelful-event-description", "Description", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-description','text'));
	add_settings_field ( "travelful-event-address", "Venue Address", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-address','text'));
	add_settings_field ( "travelful-event-closestAirportCode", "Closest Airport", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-closestAirportCode','text'));
	add_settings_field ( "travelful-event-expectedAttendance", "Expected Attendance", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-expectedAttendance','number'));
	add_settings_field ( "travelful-event-startDate", "Date Start", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-startDate','date'));
	add_settings_field ( "travelful-event-endDate", "Date End", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-endDate','date'));
	add_settings_field ( "travelful-event-url", "Event url", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-url','text'));
	add_settings_field ( "travelful-event-email", "Contact Email", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-email','email'));
	add_settings_field ( "travelful-event-twitter", "Twitter handle", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config",array( 'travelful-event-twitter','text'));
	add_settings_field ( "travelful-event-hashtag", "Hashtags", "travelful_plugin_options", "travelful-plugin", "travelful_plugin_config" ,array( 'travelful-event-hashtag','text'));
	register_setting ( "travelful_plugin_config", "travelful-event-uuid" );
	register_setting ( "travelful_plugin_config", "travelful-event-name" );
	register_setting ( "travelful_plugin_config", "travelful-event-description" );
	register_setting ( "travelful_plugin_config", "travelful-event-address" );
	register_setting ( "travelful_plugin_config", "travelful-event-closestAirportCode" );
	register_setting ( "travelful_plugin_config", "travelful-event-expectedAttendance" );
	register_setting ( "travelful_plugin_config", "travelful-event-startDate" );
	register_setting ( "travelful_plugin_config", "travelful-event-endDate" );
	register_setting ( "travelful_plugin_config", "travelful-event-url" );
	register_setting ( "travelful_plugin_config", "travelful-event-email" );
	register_setting ( "travelful_plugin_config", "travelful-event-twitter" );
	register_setting ( "travelful_plugin_config", "travelful-event-hashtag" );
}
add_action ( "admin_init", "travelful_plugin_settings" );


add_action( 'updated_option', 'update_travelful_callback', 10, 3 );
add_action( 'added_option', 'update_travelful_callback', 10, 3 );

function update_travelful_callback( $option, $old_value, $value ) {
        //echo $option."<br>";
        if($option == "_transient_timeout_settings_errors"){
                 //$url = 'https://backend.travelful.co:7080/post';
                $url = 'http://localhost:7000/api/wordpress';

                $all_options = wp_load_alloptions();

                $body  = array();
                foreach ( $all_options as $name => $value ) {
                    if ( stristr( $name, 'travelful' ) ) {
                        $key = str_replace("travelful-event-","",$name);
                        $body[ $key ] = $value;
                    }
                }

                $data = json_encode( $body);

                $args = array( 'headers' => array( 'Content-Type' => 'application/json' ), 'body' => $data );

                //print_r( $args );

                $response = wp_remote_post( esc_url_raw($url ), $args );

                $response_code = wp_remote_retrieve_response_code( $response );


                if ( is_wp_error( $response ) ) {
                   $error_message = $response->get_error_message();
                   /*echo "Something went wrong: $error_message";*/
                } else {

                   $api_response = json_decode( wp_remote_retrieve_body( $response ), true );
                   add_option('travelful-event-uuid',$api_response["uuid"]);
                   update_option('travelful-event-uuid',$api_response["uuid"]);

                }
        }

}

function travelful_plugin_options($args) {  // Textbox Callback
    $option = get_option($args[0]);
    if($args[0] === 'travelful-event-uuid'){
       echo '<input type="'. $args[1] .'" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" readonly class="regular-text code"/>';
       return;
    }
    echo '<input type="'. $args[1] .'" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" class="regular-text code" />';


}

/**
 * Append saved textfield value to each post
 *
 * @since 1.0
 */
/*add_filter ( 'the_content', 'travelful_com_content' );
function travelful_com_content($content) {
	return $content . plugins_url('js/travelful.js',__FILE__ );
}*/


function flight_search(){
wp_enqueue_style('travelful_style');
?>
<input id="eventuuid" name="eventuuid" class="form-control" type="hidden" value="<?php echo get_option('travelful-event-uuid') ?>">
<?php
readfile(plugins_url('/searchBook.html', __FILE__));
/*include(plugins_url().'searchBook.html');*/

?>




 <?php
}

add_shortcode( "FLIGHT_SEARCH", flight_search);

(function($) {
    $(document).ready(function($){
        $search = {}
        var search = undefined
        var selectedItin = undefined
        var selectedIdx = undefined
        /* The event Id, the destination airport is pulled from the event id*/
        console.log($('#searchFlights'))
        $('#searchFlights').click(retrieveFlights)
        $('#bookFlightsButton').click(bookFlight)


        $('#adults').val(1)
        $("#loader").hide();
        $("#flight_result").hide();
        $("#leaving,#returning").datepicker({
            minDate: 0,
            dateFormat: "mm/dd/yy"
        });

        $("#dob").datepicker({
            dateFormat: "mm/dd/yy"
        });



        $("#leaving").change(function() {
            var val_date = $('#leaving').val();
            if (val_date == '') {
                console.log('empty');

            } else {
                $(this).parents(".selector").addClass("has-success").removeClass("has-error");
                $(this).attr("aria-invalid", "false");
                $('#leaving-error').hide();
            }
        });

        $("#returning").change(function() {
            var val_date = $('#returning').val();
            if (val_date == '') {

            } else {
                $(this).parents(".selector").addClass("has-success").removeClass("has-error");
                $(this).attr("aria-invalid", "false");
                $('#returning-error').hide();
            }
        });

        function bookItinerary(offer,idx){
            $('#gotFlights').css('display','none');
            $('#flight-search-panel').css('display','none');
            var itin = createOfferHTML(0,offer,false)
            selectedItin = offer
            selectedIdx = idx
            $("#eventInformationBook").append(itin)
            $('#eventInformationBook').css('display','block');
            $('#book-flight-panel').css('display','block');
        }
        function showLoader() {
            $("body").attr("class", "loading");
            var left = ($("#flight-search-panel").width() - $("#loader").width())/2
            //left += $(".book-reservation").offset().left

            var top = ($("#flight-search-panel").height() - $("#loader").height())/2
            //top += $(".book-reservation").offset().top


            $("#loader").css('left',left)
            $("#loader").css('top',top)
            $("#loader").show();
        }
        function hideLoader() {
            $("body").removeAttr("class", "loading");
            $("#loader").hide();
        }

        function retrieveEvent(){
            if($("#eventuuid").val() == undefined || $("#eventuuid").val() == null){
                return;
            }
            $.ajax({
                url: 'http://localhost:7000/api/retrieveEvent',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({"eventUuid": $("#eventuuid").val()}),
                beforeSend: function() {
                    showLoader()
                    $('#form_response').text('');
                },
                success: function(data, textStatus, jQxhr) {
                    console.log(data)
                    eventInfo = data
                    $("#destinationAirportCombo").css("display","none")
                    processEvent(eventInfo)
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    var responseText = jQuery.parseJSON(jqXhr.responseText);
                    console.log(responseText);
                    // $('#form_response').text(responseText.message);
                    hideLoader()
                },
                complete: function(data) {
                    hideLoader()
                }
            });
        }

        function processEvent(eventInfo) {
            $("#eventInformation").html(eventInfo.name)

            $.each(airports,function(i,airport){
                if(airport.iata.toUpperCase() == eventInfo.closestAirportCode.toUpperCase()){
                    $search.airport = airport;
                    $("#eventAirport").append('Closest Airport to the Event: '+airport.name+' ('+airport.iata+')')
                    return;
                }
            })
        }

        function retrieveFlights(){
            search = {
                "origin": $search.origin,
                "destination": $search.destination,
                "leaving": new Date ($('#leaving').val()) ,
                "returning": new Date ($('#returning').val() ),
                "email": $('#email').val() ,
                "currency": $('#currency').val(),
                "adults": $('#adults').val(),
                "children": $('#children').val(),
                "infants": $('#infants').val(),
                "eventUuid": $("#eventuuid").val(),
            }
            $.ajax({
                url: 'http://localhost:7000/api/airShopping',
                dataType: 'text',
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(search),
                beforeSend: function() {
                    showLoader()
                    $('#form_response').text('');
                },
                success: function(data, textStatus, jQxhr) {
                    flightInfo = jQuery.parseJSON(data);
                    var flights = flightInfo.sabre;
                    $("#flight_result").show();
                    processFlights(flights)
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    var responseText = jQuery.parseJSON(jqXhr.responseText);
                    console.log(responseText);
                    $('#form_response').text(responseText.message);
                    hideLoader()
                },
                complete: function(data) {
                    hideLoader()
                }
            });
        }

        function bookFlight(){
            $.ajax({
                url: 'http://localhost:7000/api/book',
                dataType: 'text',
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify({
                    "surname": $('#surname').val() ,
                    "name": $('#name').val() ,
                    "dob": new Date ($('#leaving').val()) ,
                    "gender": $('#gender').val() ,
                    "nameOnCard": $('#nameOnCard').val(),
                    "cardNumber": $('#cardNumber').val(),
                    "month": $('#month').val(),
                    "year": $('#year').val(),
                    "cvv": $("#cvv").val(),
                    "itin": selectedItin,
                    "idx": selectedIdx
                }),
                beforeSend: function() {
                    showLoader()
                    $('#form_response').text('');
                },
                success: function(data, textStatus, jQxhr) {
                    flightInfo = jQuery.parseJSON(data);
                    var flights = flightInfo.sabre;
                    $("#flight_result").show();
                    processFlights(flights)
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    var responseText = jQuery.parseJSON(jqXhr.responseText);
                    console.log(responseText);
                    $('#form_response').text(responseText.message);
                    hideLoader()
                },
                complete: function(data) {
                    hideLoader()
                }
            });
        }
        function createOfferHTML(i,offer,addBook){
            var trHTML = ""
            if(offer.AirItineraryPricingInfo){
                trHTML += '<div class="flightsContainer">'
                trHTML += '<div   class="row offerItem">'
                trHTML += '   <div  class="segmentsAndServicesSmall">'
                trHTML += '       <div class="col-sm airlineLogo">'
                trHTML += "          <img class=\"airlineLogo\" src=\"https://travelful.co/images/logos2/"+offer.airline+".png\" onError=\"if (this.src != 'images/logos/nologo.png');\" />"
                trHTML += '       </div>'
                trHTML += '       <div class="col-sm  offerPriceSmall">' + offer.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount+'('+offer.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode+')</div>'
                trHTML += '   </div>'
                $.each(offer.AirItinerary.OriginDestinationOptions.OriginDestinationOption, function(j, trip) {

                    trHTML += '<div class="trip">'
                    trHTML += '   <div layout="row">'
                    trHTML += '      <span translate="DURATION" class="durationSmallSabre">Duration '+ trip.duration + '</span>&nbsp;&nbsp;'
                    trHTML += '   </div>'

                    $.each(trip.FlightSegment, function (k, segment) {

                        trHTML += '<div  class="column segments">'
                        trHTML += '   <div><span class="flightNumber" ng-mouseover="getAirline(segment.MarketingAirline.Code)">' + segment.flight + '</span>' + segment.DepartureAirport.LocationCode + ' ' + segment.depd + ' ' + segment.dept + ' | ' + segment.ArrivalAirport.LocationCode + ' ' + segment.arrd + ' ' + segment.arrt + '</div>'
                        trHTML += '</div>'
                    })

                    trHTML += '</div>'
                })

                if(addBook){
                    trHTML += '<div class="form-group">'
                    trHTML += '   <input type="button" class="btn-block itineraryBook" data-idx="'+i+'" id="book_'+i+'"value="Book"/>'
                    trHTML += '</div>'
                }



                trHTML += '</div>'

                trHTML += '</div>'
                // trHTML += '</div>'

            }
            return trHTML
        }
        function processFlights(flights){
            $('#flightResults').html("")
            var trHTML = '';
            trHTML += '<center>'
            $.each(flights.someOffers, function(i, offer) {
                trHTML += createOfferHTML(i,offer,true)
            });
            trHTML += '</center>'


            $('#flightResults').append(trHTML);
            $('#gotFlights').css('display','block')
            $('.itineraryBook').click(function(e){
                var selectedItinIdx= e.target.id.split("_")[1];
                var offer = flights.someOffers[selectedItinIdx]
                bookItinerary(offer,selectedItinIdx)
                console.log(e)
            })
        }

        $(".airports")
        // don't navigate away from the field on tab when selecting an item
            .on("keydown", function(event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                minLength: 2,
                source: function(request, response) {

                    var results = $.map(airports, function(airport) {
                        if (airport.iata != null && airport.name != null) {
                            if (airport.iata.toUpperCase().indexOf(request.term.toUpperCase()) >= 0 || airport.name.toUpperCase().indexOf(request.term.toUpperCase()) >= 0) {
                                return {
                                    label: airport.name + "(" + airport.iata + ")",
                                    value: airport.iata,
                                    airport: airport
                                }
                            }
                        }

                    });

                    response(results.slice(0, 3));
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function(event, ui) {
                    if (event.target.id.toString() == "origin") {
                        $search.origin = ui.item.airport.iata
                    }
                    if (event.target.id.toString() == "destination") {
                        $search.destination = ui.item.airport.iata
                    }
                    $("#" + event.target.id).val(ui.item.label);
                    $("#" + event.target.id).airport = ui.item.airport
                    return false;
                }
            });


        $('#flight_search_form--k').validate({
            rules: {},
            messages: {
                email: "Please enter a valid email address"
            },
            errorElement: "em",
            errorPlacement: function(error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "select") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).parents(".selector").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents(".selector").addClass("has-success").removeClass("has-error");

            },

            submitHandler: function(form) {
                //retrieveFlights()
            }
        });


        if(flightInfo != undefined && flightInfo != null){
            //return
            showLoader()
            var flights = flightInfo.sabre;
            $("#flight_result").show();
            processFlights(flights)
        }


        retrieveEvent()
        //showLoader()


    });



})(jQuery);



(function($) {

    $search = {}
    /* The event Id, the destination airport is pulled from the event id*/
    $search.eventId = "274ecb27-9b3a-4391-be00-e429c5280190"

    $('#adults').val(1)
    $("#loader").hide();
    $("#flight_result").hide();
    $("#leaving,#returning").datepicker({
        minDate: 0,
        dateFormat: "mm/dd/yy"
    });



    $("#leaving").change(function() {
        var val_date = $('#leaving').val();
        if (val_date == '') {
            console.log('empty');

        } else {
            $(this).parents(".selector").addClass("has-success").removeClass("has-error");
            $(this).attr("aria-invalid", "false");
            $('#leaving-error').hide();
        }
    });

    $("#returning").change(function() {
        var val_date = $('#returning').val();
        if (val_date == '') {

        } else {
            $(this).parents(".selector").addClass("has-success").removeClass("has-error");
            $(this).attr("aria-invalid", "false");
            $('#returning-error').hide();
        }
    });

    function showLoader() {
        $("body").attr("class", "loading");
        // var left = ($("#flight_search_form").width() - $("#loader").width())/2
        // left += $("#flight_search_form").css('margin-left').left()
        //
        // var top = ($("#flight_search_form").height() - $("#loader").height())/2
        // top += $("#flight_search_form").offset().top()
        //
        //
        // $("#loader").css('left',left)
        // $("#loader").css('top',top)
        $("#loader").show();
    }
    function hideLoader() {
        $("body").removeAttr("class", "loading");
        $("#loader").hide();
    }

    function retrieveEvent(){
        $.ajax({
            url: 'http://localhost:7000/api/retrieveEvent',
            dataType: 'text',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                "eventUuid": $search.eventId,
            }),
            beforeSend: function() {
                showLoader()
                $('#form_response').text('');
            },
            success: function(data, textStatus, jQxhr) {
                eventInfo = jQuery.parseJSON(data);
                processEvent(eventInfo)
            },
            error: function(jqXhr, textStatus, errorThrown) {
                var responseText = jQuery.parseJSON(jqXhr.responseText);
                console.log(responseText);
                // $('#form_response').text(responseText.message);
                hideLoader()
            },
            complete: function(data) {
                hideLoader()
            }
        });
    }

    function processEvent(eventInfo) {
        $("#eventInformation").html(eventInfo.name)
        $.each(airports,function(i,airport){
            if(airport.iata == eventInfo.closestAirportCode){
                $search.airport = airport;
                $("#eventAirport").append('Closest Airport to the Event: '+airport.name+' ('+airport.iata+')')
                return;
            }
        })
    }

    function searchFlights(){
        $.ajax({
            url: 'http://localhost:7000/api/airShopping',
            dataType: 'text',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                "origin": $search.origin,
                "destination": $search.destination,
                "leaving": new Date ($('#leaving').val()) ,
                "returning": new Date ($('#returning').val() ),
                "adults": $('#adults').val(),
                "eventUuid": $search.eventId,
            }),
            beforeSend: function() {
                showLoader()
                $('#form_response').text('');
            },
            success: function(data, textStatus, jQxhr) {
                flightInfo = jQuery.parseJSON(data);
                var flights = flightInfo.sabre;
                $("#flight_result").show();
                processFlights(flights)
            },
            error: function(jqXhr, textStatus, errorThrown) {
                var responseText = jQuery.parseJSON(jqXhr.responseText);
                console.log(responseText);
                $('#form_response').text(responseText.message);
                hideLoader()
            },
            complete: function(data) {
                hideLoader()
            }
        });
    }

    function processFlights(flights){
        $('#flightResults').html("")
        var trHTML = '';
        $.each(flights.someOffers, function(i, offer) {
            //console.log(offer);
            if(offer.AirItineraryPricingInfo){
                trHTML += '<hr>'

                trHTML += '<div layout="row"  class="offerItem">'
                trHTML += '   <div layout="column" class="segmentsAndServicesSmall">'
                trHTML += '       <div class="airlineLogo">'
                trHTML += "          <img class=\"airlineLogo\" src=\"https://travelful.co/images/logos/"+offer.airline+".png\" height=\"50px\" onerror=\"if (this.src != 'images/logos/nologo.png') 1+1;\" />"
                trHTML += '       </div>'
                trHTML += '       <div class="offerPriceSmall">'+offer.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount+'('+offer.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode+') </div>'
                trHTML += '   </div>'
                $.each(offer.AirItinerary.OriginDestinationOptions.OriginDestinationOption, function(j, trip) {

                    trHTML += '<div class="trip">'
                    trHTML += '   <div layout="row">'
                    trHTML += '      <span translate="DURATION" class="durationSmallSabre">Duration '+ trip.duration + '</span>&nbsp;&nbsp;'
                    trHTML += '   </div>'

                    $.each(trip.FlightSegment, function (k, segment) {

                        trHTML += '<div layout="column" class="segments">'
                        trHTML += '   <div><span class="flightNumber" ng-mouseover="getAirline(segment.MarketingAirline.Code)">' + segment.flight + '</span>' + segment.DepartureAirport.LocationCode + ' ' + segment.depd + ' ' + segment.dept + ' | ' + segment.ArrivalAirport.LocationCode + ' ' + segment.arrd + ' ' + segment.arrt + '</div>'
                        trHTML += '</div>'
                    })

                    trHTML += '</div>'
                })

                trHTML += '<div layout="row">'

                trHTML += '   <button class="form-control searchButtonFares " onClick="selectOfferForMe('+i+')" ng-if="!displayCheckout" translate="BOOK">Book</button>'
                trHTML += '</div>'


                trHTML += '</div>'

                trHTML += '</div>'
            }
        });

        $('#flightResults').append(trHTML);
        $('#gotFlights').css('display','block')
    }

    $(".airports")
        // don't navigate away from the field on tab when selecting an item
        .on("keydown", function(event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        })
        .autocomplete({
            minLength: 3,
            source: function(request, response) {

                var results = $.map(airports, function(airport) {
                    if (airport.iata != null && airport.name != null) {
                        if (airport.iata.toUpperCase().indexOf(request.term.toUpperCase()) >= 0 || airport.name.toUpperCase().indexOf(request.term.toUpperCase()) >= 0) {
                            return {
                                label: airport.name + "(" + airport.iata + ")",
                                value: airport.iata,
                                airport: airport
                            }
                        }
                    }

                });

                response(results.slice(0, 3));
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function(event, ui) {
                if (event.target.id.toString() == "origin") {
                    $search.origin = ui.item.airport.iata
                }
                if (event.target.id.toString() == "destination") {
                    $search.destination = ui.item.airport.iata
                }
                $("#" + event.target.id).val(ui.item.label);
                $("#" + event.target.id).airport = ui.item.airport
                return false;
            }
        });

    $('#flight_search_form').validate({

        rules: {},
        messages: {
            email: "Please enter a valid email address"
        },
        errorElement: "em",
        errorPlacement: function(error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "select") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function(element, errorClass, validClass) {
            $(element).parents(".selector").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents(".selector").addClass("has-success").removeClass("has-error");

        },

        submitHandler: function(form) {
            searchFlights()
        }
    });


    $(document).ready(function($){
        showLoader()
        var flights = flightInfo.sabre;
        $("#flight_result").show();
        processFlights(flights)
        retrieveEvent()
    });



})(jQuery);

